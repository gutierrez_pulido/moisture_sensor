import sys
import os
import logging
from pprint import pprint

from moisture_sensor.tools.cli_utils import get_value_from_user, \
    accept_input, end, not_understood, clear_screen, \
    wait_for_input

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from moisture_sensor.main import send_email
from moisture_sensor.sensors import sensors
from moisture_sensor._database import get_db_session
from moisture_sensor.reports.settings import SettingDBInteractor
from moisture_sensor.tools.calibrate_sensor import calibration_menu

def add_user():
    clear_screen()

    session = get_db_session()
    setts = SettingDBInteractor(session=session)

    print("Setting new user: ")
    email = get_value_from_user("email:")
    name = get_value_from_user("name: ")
    last_name = get_value_from_user("[optional] last name: ")

    setts.add_new_user(email=email, first_name=name, last_name=last_name)


def subscribe_to_sensor():
    clear_screen()

    session = get_db_session()
    setts = SettingDBInteractor(session=session)

    print("Subscribing user to sensors: ")
    email = get_value_from_user("user email:")
    print("list sensors to be subscribed to like so: '1,2-5' ")
    pins = input("sensor pins: ")
    all_pins = set()
    for pin in pins.strip().split(","):
        if "-" in pin:
            start, end = pin.split("-")
            all_pins.update(range(int(start), int(end) + 1))
        elif len(pin) > 1:
            print(f"pins {pin} not understood. skipping")
        else:
            all_pins.add(int(pin))

    print(f"subcribing to: {sorted(all_pins)}")
    for pin_index in all_pins:
        try:
            setts.subscribe_to_sensor(sensor_pin=pin_index, user_email=email)
        except ValueError:
            print(f"sensor pin: {pin_index} does not exist")


def register_sensor():
    clear_screen()

    print("Starting Sensor registration:")
    message = "type the name of the sensor: "
    name = get_value_from_user(message)

    pin = ask_for_pin()
    if pin < 0:
        return

    session = get_db_session()
    try:
        sensors.add_sensor(session=session, pin_number=pin, name=name)
    except ValueError as err:
        print(err)

    wait_for_input()


def ask_for_pin():
    correct = False
    pin = -1
    while not correct:
        pin_str = input("type the pin of the sensor: ")
        pin_str = pin_str.strip()
        try:
            pin = int(pin_str)
        except ValueError:
            print("please use integers only. \n\r"
                  "if you want to cancel use a negative number")
            continue
        correct = accept_input(pin)
    return pin


def decommission_sensor():
    clear_screen()

    session = get_db_session()
    print("you are about to deactivate a sensor.")

    pin = ask_for_pin()
    if pin < 0:
        return
    sense = sensors.get_sensor(session, pin)
    print("about to decomission this sensor: ")
    pprint(sense)
    accept = accept_input(pin)
    if not accept:
        return
    sensors.deactivate_sensor(session, pin_number=pin)
    print("success")
    wait_for_input()


def list_sensors():
    clear_screen()

    print("listing sensors: ")
    session = get_db_session()
    all_sensors = sensors.list_all_sensors(session, include_inactive=False)
    if not all_sensors:
        print("no sensors found")
        wait_for_input()
        return
    for sense in all_sensors:
        pprint(sense)
        print("---")
    wait_for_input()


commands = {
    "1": register_sensor,
    "2": decommission_sensor,
    "3": list_sensors,
    "4": add_user,
    "5": subscribe_to_sensor,
    "6": send_email,
    "7": calibration_menu,
    "q": end,

}


def main_menu():
    print('===========================')
    print("Select an option:")
    print("1) register a sensor")
    print("2) decommission a sensor")
    print("3) list sensors.")
    print("4) add user")
    print("5) subscribe to sensors")
    print("6) send emails.")
    print("7) calibrate_sensors.")

    print("Q => press Q to quit")

    selection = input("enter your selection: ").strip().lower()
    print(f"selection: was {selection}")

    selected_command = commands.get(selection, not_understood)
    selected_command()


if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"

    logging.basicConfig(filename="log.txt", format=format, level=logging.DEBUG,
                        datefmt="%H:%M:%S")

    # create logger with 'spam_application'
    logger = logging.getLogger('moisture_sensor')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('log.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    print("Welcome.")
    try:
        while True:
            main_menu()

    except KeyboardInterrupt:
        print("recieved kill command. closing down shop")
        quit()
