import pygatt
import time
from typing import Callable
import logging

from moisture_sensor.recieve_readings import parser

adapter = pygatt.GATTToolBackend()


RX_CHARACTERISTIC = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
TX_CHARACTERISTIC = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"

DEVICE_MAC_ADDRESS = "DE:B1:1D:2D:BA:30"


def start_sensor_reciever(callback, tx_characteristic, device_mac_address):
    # type: (Callable, str, str) -> pygatt.backends.BLEBackend
    logging.info("starting adapter")
    adapter.start()
    time.sleep(1)
    logging.info("done")

    time.sleep(2)
    logging.info(f"attempting to connect to {device_mac_address}")
    device = adapter.connect(device_mac_address,
                             timeout=20,
                             address_type=pygatt.BLEAddressType.random)
    logging.info("connected")
    time.sleep(1)
    logging.info("subscribing")
    device.subscribe(tx_characteristic,
                     callback=callback)
    time.sleep(1)
    return adapter


def recieve_bluetooth_sensor_data(callback, tx_characteristic, device_mac_address):
    # type: (parser.BluetoothLineHandler, str) -> None
    global adapter
    try:
        adapter = start_sensor_reciever(callback, tx_characteristic, device_mac_address)

        while not callback.message_sent:
            # hold the thread alive to recieve data
            pass
    except KeyboardInterrupt:
        logging.info("kill command recieved: \n\t")
        quit(0)
    finally:
        logging.info("stopping adapter")
        if adapter is not None:
            adapter.stop()
            adapter.kill()
