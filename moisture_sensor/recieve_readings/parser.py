import logging
import json

logger = logging.getLogger('moisture_sensor')


class RegisterInterface:
    def make_record(self, value):
        # type: (str) -> None
        raise NotImplementedError("responsibility of the child class")


class BluetoothLineHandler:
    MESSAGE_END_CHAR = "\n"

    def __init__(self, register):
        # type: (RegisterInterface) -> None
        self.register = register
        self.message = ""
        self.message_sent = False

    def recieve_data(self, handle, value):
        # type: (int, bytearray) -> None
        """
        handle -- integer, characteristic read handle the data was received on (required for compliance
            with the reciever)
        value -- bytearray, the data returned in the notification
        """
        value = value.decode("utf-8")
        if value == self.MESSAGE_END_CHAR:
            logger.debug("got message {}, making record.".format(self.message))
            self.register.make_record(self.message)
            self.message_sent = True
            self.message = ""
        else:
            self.message += value

    def __call__(self, *args, **kwargs):
        self.recieve_data(*args, **kwargs)


class JSONDictHandler:
    def __init__(self, register):
        # type: (RegisterInterface) -> None
        self.register = register
        self.message = ""

    def recieve_data(self, handle, value):
        # type: (int, bytes) -> None
        """ converts the data into a format the register can use

        :param handle: ??? kept for compatibility
        :param value: bytes, string repersentation of the data
        :return: None
        """
        data = "".join(f"{key}, {val}." for key, val in json.loads(value).items())
        self.register.make_record(data)

    def __call__(self, *args, **kwargs):
        self.recieve_data(*args, **kwargs)
