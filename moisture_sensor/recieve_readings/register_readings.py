"""
Module dedicated to persisting data
"""
from datetime import datetime
import logging
from moisture_sensor.recieve_readings.parser import RegisterInterface

logger = logging.getLogger('moisture_sensor')

__all__ = ["RegisterReading"]


class RegisterReading(RegisterInterface):
    def __init__(self, db_interactor):
        # type: (ReadingDBInterface) -> None
        self.db = db_interactor

    def make_record(self, value):
        for data_pair in value.split("."):
            logger.info(data_pair)
            try:
                sensor, reading = [int(x.strip()) for x in data_pair.split(",")]
            except ValueError:
                logger.error(f"not enough values to unpack in \"{data_pair}\" ")
                # not enough values to unpack means end of string
                continue
            try:
                self.db.add_reading(sensor=sensor, reading=reading, date_of_reading=self.get_date())
            except Exception as e:
                logger.error(e)

    def get_date(self):
        return datetime.now()


class ReadingDBInterface:
    def add_reading(self, sensor: int, reading: int, date_of_reading: datetime):
        raise NotImplementedError("responsibility of child class")


