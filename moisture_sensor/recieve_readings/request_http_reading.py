import requests

from moisture_sensor.recieve_readings import parser


def do_request(callback, ip):
    # type: (parser.JSONDictHandler) -> None
    """get the data from the sensors server.

    the server returns a json dictionary in bytes.
    """
    myURL = requests.get(ip)
    callback(1, myURL.content)
