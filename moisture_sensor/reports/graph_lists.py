from dataclasses import dataclass
from sqlalchemy.exc import IntegrityError
from typing import List

from moisture_sensor import _database as db
from moisture_sensor.sensors import sensors


@dataclass
class GraphList:
    list_id: int = 0
    user_id: int = 0
    name: str = str
    sensors: list = list

    def from_orm_object(self, value):
        self.list_id = value.list_id
        self.user_id = value.user_id
        self.name = value.name
        self.sensors = [sensor.pin_number for sensor in value.sensors]


class GraphListDBInteractor:
    def __init__(self, session):
        self.session = session

    def add_new_graph_list(self, user_email: str, sensor_pins: list, list_name: str):
        """create a new graph list"""
        try:
            user = self._get_orm_user(user_email=user_email)

            new_list = db.GraphList(user_id=user.user_id, name=list_name)
            found_sensors = self._get_sensors(sensor_pins)

            new_list.sensors = found_sensors
            self.session.add(new_list)
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            raise ValueError("User already has a table under that name")
        return new_list.list_id

    def _get_sensors(self, sensor_pins):
        found_sensors = []
        for pin in sensor_pins:
            try:
                sensor = sensors.query_sensor_object(
                    session=self.session, pin_number=pin)
                found_sensors.append(sensor)
            except ValueError as e:
                print(f"encountered an error when searching for pin {pin}")
                print(e)

        return found_sensors

    def get_user_graph_lists(self, user_email: str) -> List[GraphList]:
        user = self._get_orm_user(user_email)
        lists = self.session.query(db.GraphList). \
            filter(db.GraphList.user_id == user.user_id)

        graph_lists = []
        for list in lists:
            n = GraphList()
            n.from_orm_object(list)
            graph_lists.append(n)

        return graph_lists

    def _get_orm_user(self, user_email):
        """returns ORM objects, for internal use only"""
        user = self.session.query(db.User) \
            .filter(db.User.email == user_email).one()
        return user

    def add_pins_to_graph_list(self, user_email: str,
                               list_name: str, sensor_pins: list):
        user = self._get_orm_user(user_email=user_email)
        list = self.session.query(db.GraphList). \
            filter(db.GraphList.user_id == user.user_id). \
            filter(db.GraphList.name == list_name).one()
        sensors = self._get_sensors(sensor_pins)

        list.sensors.extend(sensors)
        self.session.commit()
