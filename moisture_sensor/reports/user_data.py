from datetime import date, timedelta
from collections import OrderedDict
from typing import Union

from sqlalchemy.orm.session import Session
from sqlalchemy.orm.exc import NoResultFound

from moisture_sensor import _database as db
from moisture_sensor.reports.settings import SettingDBInteractor
from moisture_sensor.readings import readings
from moisture_sensor.reports.format_data import UserDataInterface
from moisture_sensor.reports.graph_lists import GraphList, GraphListDBInteractor


class AllUserSensors(UserDataInterface):
    def __init__(self, session: Session, user_email: str):
        super().__init__(session, user_email)
        self.session = session
        self._reading = {}
        self._sensors = None

        try:
            self._user = session.query(db.User).\
                filter(db.User.email == user_email).one()   # type: db.User
        except NoResultFound:
            raise ValueError(f"No user \"{user_email}\" found")

        user_settings = SettingDBInteractor(session).\
            list_user_settings(self._user.email)
        self.user_settings = user_settings
        self.email_frequency = user_settings.email_frequency
        self.email = user_settings.email
        self.days_in_graph = user_settings.days_in_graph
        self.first_name = self._user.first_name
        self.last_name = self._user.last_name

    @property
    def subscribed_sensors(self):
        if self._sensors is None:
            self._sensors = self.user_settings.subscribed_sensors
        return self._sensors

    @subscribed_sensors.setter
    def subscribed_sensors(self, value):
        self._sensors = value

    @property
    def readings(self):
        reads = readings.SensorReadingDBInteractor(self.session)

        data = OrderedDict()
        for sensor in self.subscribed_sensors:
            pin_number = sensor["pin_number"]
            data[pin_number] = [[read["date"], read["value"]]
                                for read in reads.get_readings(
                    pin_number, after_date=self._start_date())]

        return data

    def _start_date(self):
        start = date.today() - timedelta(days=self.days_in_graph)
        return start


class UserMailList(AllUserSensors):
    """Collects the sensors in a mailing list"""
    def __init__(self, session: Session, user_email: str, mail_list: Union[str, GraphList]):
        super().__init__(session=session, user_email=user_email)

        self.graph_list = self._get_user_list(mail_list)
        self.mail_list = self.graph_list.name

    def _get_user_list(self, mail_list:  Union[str, GraphList]) -> GraphList:
        if isinstance(mail_list, str):
            my_list = None
            graphs = GraphListDBInteractor(session=self.session, )
            graph_lists = graphs.get_user_graph_lists(self.email)
            for list in graph_lists:
                if list.name == mail_list:
                    return list
            else:
                raise ValueError(f"No Graph list under the name \'{mail_list}\' found")
        elif isinstance(mail_list, GraphList):
            return mail_list

    @property
    def subscribed_sensors(self):
        if self._sensors is None:
            self._sensors = [sensor for sensor in self.user_settings.subscribed_sensors
                             if sensor.get("pin_number") in self.graph_list.sensors]

        return self._sensors

    @subscribed_sensors.setter
    def subscribed_sensors(self, value):
        self._sensors = value
