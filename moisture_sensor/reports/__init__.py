
__all__ = []


class PlotInterface:
    def __init__(self,  user_data):
        pass

    def save_to(self, image_path: str):
        raise NotImplementedError("responsibility of subclass")

    def show(self):
        raise NotImplementedError("responsibility of subclass")

