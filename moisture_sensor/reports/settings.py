from typing import Optional
from dataclasses import dataclass
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from moisture_sensor import _database as db
from moisture_sensor.sensors import sensors


@dataclass
class UserSettings:
    email: str
    email_frequency: int
    subscribed_sensors: list
    days_in_graph: int


class SettingDBInteractor():
    def __init__(self, session):
        self.session = session

    def add_new_user(self, email: str, first_name: str, last_name: Optional[str] = None):
        new_user = db.User(email=email, first_name=first_name, last_name=last_name)
        self.session.add(new_user)
        try:
            self.session.commit()
        except IntegrityError:
            raise ValueError("Email already exists. Please use a new email")

    def list_users(self):
        data = []
        for user in self.session.query(db.User).all():
            data.append(self._user_to_dict(user))
        return data

    def _user_to_dict(self, user):
        return {"email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name}

    def set_email_frequency(self, user_email: str, frequency: int):
        """set the frequency of reports for a user.
        :argument user_email: email
        :argument frequency: int, number of days between reports
        """
        user = self._get_user(user_email)
        user_settings = self._get_user_settings(user.user_id)

        user_settings.email_frequency = frequency
        self.session.commit()

    def _get_user_settings(self, user_id):
        try:
            user_settings = self.session.query(db.Settings).\
                filter(db.Settings.user_id == user_id).one()
        except NoResultFound:
            user_settings = db.Settings(user_id=user_id)
            self.session.add(user_settings)
            self.session.commit()
        return user_settings

    def _get_user(self, user_email):
        return self.session.query(db.User) \
            .filter(db.User.email == user_email).one()

    def list_user_settings(self, user_email: str) -> UserSettings:
        """get a dictionary with the user settings"""
        user = self._get_user(user_email)
        user_settings = self._get_user_settings(user.user_id)

        subscribed_sensors = []
        for sensor in user.sensor_subscriptions:
            if sensor.active:
                subscribed_sensors.append(sensors.sensor_to_dict(sensor))
        return UserSettings(email=user.email,
                            email_frequency=user_settings.email_frequency,
                            subscribed_sensors=subscribed_sensors,
                            days_in_graph=user_settings.days_in_graph)

    def subscribe_to_sensor(self, user_email: str, sensor_pin: int):
        user = self._get_user(user_email)
        desired_sensor = sensors.query_sensor_object(session=self.session,
                                                     pin_number=sensor_pin)
        user.sensor_subscriptions.append(desired_sensor)
        self.session.commit()

