import smtplib
import ssl
import socket
from datetime import date
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import List

from moisture_sensor.reports import user_data, format_data
from moisture_sensor.reports.graph_lists import GraphListDBInteractor, GraphList

from sqlalchemy.orm import Session


class EmailForUser:
    def __init__(self, session: Session, reciever_email: str):
        self.message = MIMEMultipart()
        self.plot_path = "/tmp/SensorsPlot.png"
        self.session = session

        self.user_data = user_data.AllUserSensors(session=self.session,
                                                  user_email=reciever_email)

        self.reciever = self.user_data.email

        self._format_message()
        self._save_plot()

    def _format_message(self):
        date_signature = date.today()
        # self.message["From"] = sender_email
        self.message["To"] = self.reciever
        self.message["Subject"] = f"Sensor Reports {date_signature.isoformat()}"

        body = "sent from machine: {user}".format(user=socket.gethostname())
        self.message.attach(MIMEText(body, "plain"))

    def _save_plot(self):
        format_data.PlotReadings(self.user_data, self.plot_path)
        self._attach_plot()

    def _attach_plot(self):
        # Open PDF file in binary mode
        with open(self.plot_path, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {self.plot_path}",
        )

        # Add attachment to message and convert message to string
        self.message.attach(part)

    def to_text(self):
        return self.message.as_string()


class EmailGraphList:
    def __init__(self, session: Session, reciever_email: str):
        self.message = MIMEMultipart()
        self.plot_patttern = "/tmp/{}.png"
        self.session = session
        self.reciever = reciever_email

        self.graph_lists = self._get_graph_lists(reciever_email)
        self.plot_paths = []
        self._format_message()

        self.add_plots()

    def _format_message(self):
        date_signature = date.today()
        # self.message["From"] = sender_email
        self.message["To"] = self.reciever
        self.message["Subject"] = f"Sensor Reports {date_signature.isoformat()}"

        body = "sent from machine: {user}".format(user=socket.gethostname())
        self.message.attach(MIMEText(body, "plain"))

    def _get_graph_lists(self, reciever_email) -> List[user_data.UserMailList]:
        _db_graphs = GraphListDBInteractor(session=self.session)
        graphs = list()
        for name in _db_graphs.get_user_graph_lists(reciever_email):
            mail_list = user_data.UserMailList(
                session=self.session, user_email=reciever_email,
                mail_list=name)
            graphs.append(mail_list)
        return graphs

    def add_plots(self):
        for user_data in self.graph_lists:
            plot_path = self.plot_patttern.format(user_data.mail_list.replace("!", ""))
            format_data.PlotReadings(user_data=user_data, output_file=plot_path, plot_title=user_data.mail_list)
            self.plot_paths.append(plot_path)
            self._attach_plot(plot_path)

    def _attach_plot(self, plot_path):
        # Open PDF file in binary mode
        with open(plot_path, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {plot_path}",
        )

        # Add attachment to message and convert message to string
        self.message.attach(part)

    def to_text(self):
        return self.message.as_string()


class EmailSender:
    """connects to the service and sends the email"""
    def __init__(self, session: Session,
                 sender_email: str,
                 sender_pass: str):
        self.smtp_server = "smtp.gmail.com"
        self.port = 587  # For starttls
        self.sender_email = sender_email
        self.password = sender_pass
        self.context = ssl.create_default_context()
        self.server = smtplib.SMTP(self.smtp_server, self.port)
        self.session = session

    def connect(self):
        # Try to log in to server
        self.server.ehlo()  # Can be omitted
        self.server.starttls(context=self.context)  # Secure the connection
        self.server.ehlo()  # Can be omitted
        self.server.login(self.sender_email, self.password)

    def send_email_to_user(self, email: str):
        mail = EmailGraphList(session=self.session, reciever_email=email)
        self.server.sendmail(self.sender_email, email, mail.to_text())

    def __del__(self):
        try:
            self.server.quit()
        except Exception:
            pass
