from datetime import date, timedelta

from moisture_sensor.reports import email, settings
from app_secrets import API_EMAIL, EMAIL_PASSWORD


class EmailService:
    def __init__(self, session):
        self.trigger_every = timedelta(days=1)
        self.last_trigger = date.today() - timedelta(days=1)
        self.session = session

    def start(self):
        self.send_emails()

    def send_emails(self):
        if not all([API_EMAIL, EMAIL_PASSWORD]):
            raise RuntimeError("Api Email or password not set in app_secrets.py.\n"
                               "After modifying the file run `git update-index --skip-worktree app_secrets.py`"
                               " to ignore it with git (so the secrets don't get exposed) ")
        try:
            setts = settings.SettingDBInteractor(session=self.session)
            today = date.today()
            sender = email.EmailSender(session=self.session,
                                       sender_email=API_EMAIL,
                                       sender_pass=EMAIL_PASSWORD)
            sender.connect()
            for user in setts.list_users():
                user_email = user["email"]
                user_settings = setts.list_user_settings(user_email)
                if timedelta(days=user_settings.email_frequency) \
                        + today > self.last_trigger:
                    sender.send_email_to_user(user_email)

        finally:
            self.last_trigger = date.today()
