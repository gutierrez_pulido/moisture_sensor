from matplotlib import pyplot, style, transforms
from matplotlib.dates import DateFormatter
from moisture_sensor.reports import PlotInterface


class UserDataInterface:
    def __init__(self, session, user_email: str):
        # self.subscribed_sensors = list()
        self.email_frequency = int()
        self.email = str()

    @property
    def readings(self) -> dict:
        raise NotImplementedError("responsibility of subclass")


class PlotReadings(PlotInterface):
    def __init__(self, user_data: UserDataInterface, output_file: str, plot_title="Humidity report"):
        self._user_data = user_data
        self.x_size = 11
        self.y_size = 8.5
        self.bbox = transforms.Bbox([[0,0], [self.x_size, self.y_size]])

        self._populate_plot()

        self.fig = pyplot.gcf()
        style.use('ggplot')
        pyplot.legend()
        pyplot.title(plot_title)
        pyplot.ylabel("Humidity %")
        pyplot.xlabel("date")

        axes = pyplot.gca()
        date_form = DateFormatter("%m-%d")
        axes.xaxis.set_major_formatter(date_form)
        self.save_to(output_file)

    def _populate_plot(self):
        for sensor_pin, reading_data in self._user_data.readings.items():
            self._add_sensor_data(reading_data, sensor_pin)

    def _add_sensor_data(self, reading_data, sensor_pin):
        sensor = self._get_sensor(sensor_pin)
        x = []
        y = []
        [(x.append(date), y.append(value)) for date, value in reading_data]
        pyplot.plot(x, y, label=sensor["name"])

    def _get_sensor(self, sensor_pin):
        return [sensor for sensor in self._user_data.subscribed_sensors
                if sensor["pin_number"] == sensor_pin][0]

    def save_to(self, image_path: str):

        self.fig.set_size_inches(self.x_size, self.y_size)
        pyplot.savefig(image_path, dpi=(250), orientation="landscape",
                       bbox_inches=self.bbox)
        pyplot.close(self.fig)

    def show(self):
        pyplot.show()

    def __del__(self):
        pyplot.close(pyplot.gcf())
