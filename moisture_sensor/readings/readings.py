from datetime import date
from sqlalchemy.orm.exc import NoResultFound
from typing import Optional

from moisture_sensor import _database
from moisture_sensor.recieve_readings.register_readings import ReadingDBInterface

__all__ = ["SensorReadingDBInteractor"]


class SensorReadingDBInteractor(ReadingDBInterface):
    def __init__(self, db_session):
        self.session = db_session

    def add_reading(self, sensor: int, reading: int, date_of_reading: date):
        sensor_in_table = self._get_sensor_in_table(sensor)
        reading = _database.Reading(sensor_id=sensor_in_table.sensor_id,
                                    value=self.raw_to_percent(sensor_in_table, reading),
                                    date=date_of_reading)
        self.session.add(reading)

    def raw_to_percent(self, sensor, value):
        """converts raw reading to percent"""
        sensor_min = sensor.air_value
        sensor_max = sensor.water_value
        out_min = 0
        out_max = 100
        return (value - sensor_min) * (out_max - out_min) / (sensor_max - sensor_min) + out_min

    def _get_sensor_in_table(self, sensor):
        try:
            sensor_in_table = self.session.query(_database.Sensor). \
                filter(_database.Sensor.pin_number == sensor). \
                filter(_database.Sensor.active == True). \
                one()
        except NoResultFound:
            raise ValueError("Sensor Pin does not exist, or is not active.")
        return sensor_in_table

    def get_readings(self, sensor: int, after_date: Optional[date] = None):
        data = []
        sensor_in_table = self._get_sensor_in_table(sensor)

        reading_query = self.session.query(_database.Reading).filter(
            _database.Reading.sensor_id == sensor_in_table.sensor_id)

        if after_date:
            reading_query = reading_query.filter(_database.Reading.date > after_date)

        for reading in reading_query.all():
            data.append({"value": reading.value,
                         "sensor": sensor_in_table.name,
                         "date": reading.date,
                         "sensor_pin": sensor_in_table.pin_number
                         })

        return data