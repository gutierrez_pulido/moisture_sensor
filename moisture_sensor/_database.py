from sqlalchemy import Column, Integer, String, \
    ForeignKey, Boolean, Table, DateTime, Float, UniqueConstraint
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, Session

Base = declarative_base()
meta = Base.metadata


sensor_subscription = Table(
    "SensorSubscription",
    Base.metadata,
    Column("sensor_id", Integer, ForeignKey("Sensor.sensor_id")),
    Column("user_id", Integer, ForeignKey("User.user_id")),
)


graph_list_sensors = Table(
    "GraphListSensors",
    Base.metadata,
    Column("graph_list_id", Integer,
           ForeignKey("GraphList.list_id"), primary_key=True),
    Column("sensor_id", Integer,
           ForeignKey("Sensor.sensor_id"), primary_key=True),

)

class Sensor(Base):
    __tablename__ = "Sensor"

    sensor_id = Column(Integer, primary_key=True)
    name = Column(String)
    pin_number = Column(Integer)
    active = Column(Boolean)
    air_value = Column(Float, default=2000)
    water_value = Column(Float, default=800)
    reading = relationship("Reading", backref="Sensor")
    graph_list_id = Column(Integer, ForeignKey("GraphList.list_id"))


class Reading(Base):
    __tablename__ = "Reading"

    reading_id = Column(Integer, primary_key=True)
    sensor_id = Column(Integer, ForeignKey("Sensor.sensor_id"))
    value = Column(Integer)
    date = Column(DateTime)


class User(Base):
    __tablename__ = "User"

    user_id = Column(Integer, primary_key=True)
    email = Column(String, unique=True)
    first_name = Column(String)
    last_name = Column(String)
    settings = relationship("Settings", backref="User")
    sensor_subscriptions = relationship("Sensor",
                                        secondary=sensor_subscription,
                                        )
    graph_lists = relationship("GraphList", backref="User")


class Settings(Base):
    __tablename__ = "Settings"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("User.user_id"))
    email_frequency = Column(Integer, default=1)
    days_in_graph = Column(Integer, default=30)


class GraphList(Base):
    __tablename__ = "GraphList"
    list_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("User.user_id"))
    name = Column(String)
    sensors = relationship("Sensor", secondary=graph_list_sensors)

    __table_args__ = (
        UniqueConstraint("name", "user_id", name="unique_user_list_name"),
    )


def connect(engine):
    meta.create_all(engine, )


def get_db_session(path="/sensor.db", echo=False):
    # type: (str, bool) -> Session
    engine = create_engine(f'sqlite://{path}', echo=echo)
    Session_ = sessionmaker()
    Session_.configure(bind=engine)
    session = Session_()
    connect(engine)
    return session
