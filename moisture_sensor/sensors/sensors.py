from sqlalchemy.orm.session import Session
from sqlalchemy.orm.exc import NoResultFound
from moisture_sensor import _database as db

__all__ = ["add_sensor", "list_all_sensors", "get_sensor", "rename_sensor", "change_sensor_pin", "deactivate_sensor"]


def add_sensor(session: Session, pin_number: int,
               name: str, active: bool = True):
    try:
        sense = _query_sensor(pin_number, session)
    except NoResultFound:
        # no sensor exists with that pin all is good
        pass
    else:
        raise ValueError("a Sensor exists with that pin already. \n"
                         "Deactivate the previous sensor or select "
                         "a different pin")

    new_sensor = db.Sensor(name=name, pin_number=pin_number, active=active)
    session.add(new_sensor)
    session.commit()


def list_all_sensors(session: Session, include_inactive=False):
    query = session.query(db.Sensor)
    if not include_inactive:
        query = query.filter(db.Sensor.active)

    return [sensor_to_dict(sense) for sense in query.order_by(db.Sensor.pin_number).all()]


def get_sensor(session: Session, pin_number: int):
    sense = query_sensor_object(pin_number, session)
    return sensor_to_dict(sense)


def query_sensor_object(pin_number, session):
    """returns the orm object. not for public use"""
    try:
        sense = session.query(db.Sensor). \
            filter(db.Sensor.pin_number == pin_number). \
            filter(db.Sensor.active). \
            one()
    except NoResultFound:
        raise ValueError("No Active Sensor exists with that pin.")
    return sense


def sensor_to_dict(sense):
    """convert from orm object to python object"""
    return {"name": sense.name,
            "pin_number": sense.pin_number,
            "active": sense.active,
            "id": sense.sensor_id}


def rename_sensor(session: Session, pin_number: int, new_name: str):
    sense = _query_sensor(pin_number, session)
    sense.name = new_name
    session.commit()


def change_sensor_pin(session: Session, pin_number: int, new_pin: int):
    sense = _query_sensor(pin_number, session)
    sense.pin_number = new_pin
    session.commit()


def set_water_value(session: Session, pin_number: int, value: float):
    sense = _query_sensor(pin_number, session)
    sense.water_value = value
    session.commit()


def set_air_value(session: Session, pin_number: int, value: float):
    sense = _query_sensor(pin_number, session)
    sense.air_value = value
    session.commit()


def _query_sensor(pin_number, session):
    sense = session.query(db.Sensor). \
        filter(db.Sensor.pin_number == pin_number). \
        filter(db.Sensor.active).one()
    return sense


def deactivate_sensor(session: Session, pin_number: int):
    sense = _query_sensor(pin_number, session)
    sense.active = False
    session.commit()
