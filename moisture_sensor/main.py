import logging
import json
import os

from moisture_sensor import _database
from moisture_sensor.recieve_readings import parser, request_http_reading
from moisture_sensor.recieve_readings.register_readings import RegisterReading
from moisture_sensor.recieve_readings.bluetooth_reciever import recieve_bluetooth_sensor_data
from moisture_sensor.readings.readings import SensorReadingDBInteractor
from moisture_sensor.reports.email_service import EmailService

logger = logging.getLogger('moisture_sensor')


def start_bluetooth_reciever():
    """start bluetooth reciever.
    reads the sensors from the json file, and reads each one in turn.

    Uses the Line_Handler to parse the data, and pass it
     to the databse to register the readings recieved."""
    print("starting bluetooth reciever service")
    session = _database.get_db_session()
    callback = parser.BluetoothLineHandler(RegisterReading(SensorReadingDBInteractor(session)))

    sensors_json = "./bin/bluetooth_sensors.json"
    path = os.path.abspath(sensors_json)
    print(f"opening {path}")
    with open(path, "r") as fr:
        data = json.load(fr)
    try:
        for sensor_dict in data:
            recieve_bluetooth_sensor_data(callback=callback, tx_characteristic=sensor_dict.get("TX"),
                                          device_mac_address=sensor_dict.get("address"))
    finally:
        session.commit()
        session.close()


def read_sensor_server():
    """use http to request a reading from sensors v2.+ """
    print("starting http requests to the sensors servers")
    session = _database.get_db_session()
    callback = parser.JSONDictHandler(RegisterReading(SensorReadingDBInteractor(session)))

    path = os.path.abspath("bin/sensors_servers.json")
    print(f"opening {path}")
    with open(path, "r") as fr:
        data = json.load(fr)
    try:
        for sensor in data:
            request_http_reading.do_request(callback=callback, ip=sensor.get("ip"))
    finally:
        session.commit()
        session.close()


def send_email():
    print("starting email service on the background")
    session = _database.get_db_session()
    try:
        service = EmailService(session)
        service.start()
    finally:
        session.close()
