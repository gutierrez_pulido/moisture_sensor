"""
helper tool to run a calibration routine
"""
import json
import os
import time
from dataclasses import dataclass
from pprint import pformat

from moisture_sensor._database import get_db_session
from moisture_sensor.recieve_readings import request_http_reading
from moisture_sensor.sensors import sensors
from moisture_sensor.tools.cli_utils import accept_input, end, not_understood, clear_screen


class JSONDictHandler:
    def __init__(self):
        # type: () -> None
        self.data = None

    def recieve_data(self, handle, value):
        # type: (int, bytes) -> None
        """ converts the data into a dictionary format

        :param handle: ??? kept for compatibility
        :param value: bytes, string repersentation of the data
        :return: None
        """
        self.data = json.loads(value)

    def __call__(self, *args, **kwargs):
        self.recieve_data(*args, **kwargs)


def _read_sensors(servers: list) -> dict:
    callback = JSONDictHandler()
    for server in servers:
        request_http_reading.do_request(callback=callback, ip=server.get("ip"))

    return callback.data


@dataclass
class PinMaxMin:
    pin: str
    air: float = float('inf')
    water: float = 0

    def set_air(self, value):
        if value < self.air:
            self.air = value

    def set_water(self, value):
        if value > self.water:
            self.water = value


def run_calibrate():
    clear_screen()
    print("enter a list of sensors to calibrate as")
    print("a comma separated list of sensors")
    print("for example:  1,3, 16 ")

    pins = get_pins_from_user()
    pins_min_max = {pin: PinMaxMin(pin) for pin in pins}

    value_type = "air"
    calibrate_for(value_type, pins_min_max)
    value_type = "water"
    calibrate_for(value_type, pins_min_max)

    for sensor in  sorted(pins_min_max.values(), key=lambda x: x.pin):
        print("compliled data:")
        print(f"Sensor {sensor.pin}: ")
        print(f"\tair value of {sensor.air}")
        print(f"\twater value of {sensor.water}")


    session = get_db_session()
    for sensor in sorted(pins_min_max.values(), key=lambda x: x.pin):
        try:
            print(f"pin  {sensor.pin} had:")
            print(f"\tair value of {sensor.air}")
            if accept_input("that"):
                sensors.set_air_value(session=session, pin_number=sensor.pin, value=sensor.air)
                session.commit()
            else:
                print("value rejected. moving on")

            print(f"pin  {sensor.pin} had:")
            print(f"\twater value of {sensor.water}")
            if accept_input("that"):
                sensors.set_water_value(session=session, pin_number=sensor.pin, value=sensor.water)
                session.commit()
            else:
                print("value rejected. moving on")
        except Exception as e:
                print("ran into troubles: ")
                print(e)
                session.rollback()
    session.close()
    input("press any key to return to menu")


def calibrate_for(value_type, pins_map):
    servers = get_servers()
    print("\n\n")
    print("************")
    print(f"starting {value_type} calibration")
    print(f"please make sure the sensor is completely covered in {value_type}")
    print("we will try reading 5 times, and grab the largest values")
    input("press any value to start.")
    i = 0
    while i < 5:
        print("reading now: ")
        data = _read_sensors(servers)
        print(pformat(data))
        for pin, value in data.items():
            if pin not in pins_map:
                continue
            if value_type == "water":
                pins_map[pin].set_water(value)
            else:
                pins_map[pin].set_air(value)
        time.sleep(2)
        i += 1


def get_servers():
    path = os.path.abspath("bin/sensors_servers.json")
    print(f"opening {path} to find the configured servers")
    with open(path, "r") as fr:
        data = json.load(fr)
    return data


def get_pins_from_user():
    correct = False
    selected = set()
    while not correct:
        selection_str = input("enter your selection: ").strip().lower()
        try:
            selected = set(map(lambda x: x.strip(), selection_str.split(",")))
            print("your selection was: ")
            print("- ", end="")
            print("\n- ".join(map(str, selected)))
        except ValueError:
            print("please use integers separated by commas only")
            continue
        correct = accept_input("that")
    return selected


def calibration_menu():
    selection = ""
    while selection.strip().lower() != "q":
        clear_screen()
        print("Welcome to the Calibration tool")
        print("please select an option:")
        print("")
        print("1) start")
        print("Q => press Q to return to the previous menu")

        selection = input("enter your selection: ").strip().lower()
        print(f"selection: was {selection}")

        selected_command = commands.get(selection, not_understood)
        selected_command()


commands = {
    "1": run_calibrate,
    "q": lambda: None,
}
