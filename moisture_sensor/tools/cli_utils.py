import os


def get_value_from_user(message):
    correct = False
    name = ""
    while not correct:
        name = input(message)
        correct = accept_input(name)
    return name


def accept_input(value):
    accepted = input(f"is {value} correct? Y/N?")
    if accepted.strip().lower().startswith("y"):
        correct = True
    else:
        correct = False
    return correct


def end():
    raise KeyboardInterrupt()


def not_understood():
    print("selection not understood. please try again")
    input("press any key to continue")


def print_spacer():
    print("\n\n")


def clear_screen():
    os.system("clear")


def wait_for_input():
    input("press enter to continue")