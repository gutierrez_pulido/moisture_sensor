#include <SPI.h>
#include <Adafruit_BLE_UART.h>

// ble connections
#define ADAFRUITBLE_REQ 10
#define ADAFRUITBLE_RDY 2
#define ADAFRUITBLE_RST 9

Adafruit_BLE_UART uart = Adafruit_BLE_UART(ADAFRUITBLE_REQ, ADAFRUITBLE_RDY, ADAFRUITBLE_RST);


//Pin connected to ST_CP of 74HC595
int latchPin = A3;
//Pin connected to SH_CP of 74HC595
int clockPin = A4;
////Pin connected to DS of 74HC595
int dataPin = A2;
////Pin connected to sensor output (A0 is a default alias)
const int SensorPin = A5;


//holders for information you're going to pass to shifting function
const int sensorCount = 16;
byte sensorPin;
byte SensorPinLUT[sensorCount][2];

//Time between readings in milliseconds
long previousMillis = 0;
long intervalBetweenReadings = 36000000 * 6 ; // 1 hour * x in milli
int delayBetweenSensor = 250;


// sensor constants
const int air_value = 450;
const int water_value = 200;
const String sensorName = "Sensor 0";


void setup() {
  // put your setup code here, to run once:
  //set pins to output because they are addressed in the main loop
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(SensorPin, INPUT);

  Serial.begin(9600);
  Serial.println("Setup in progress");

  // setup ble
  uart.setRXcallback(rxCallback);
  uart.setACIcallback(aciCallback);
  uart.setDeviceName("Plants"); /* 7 characters max! */
  uart.begin();

  Serial.println("populating lut");
  populateLUT();
    
  blink();
  Serial.print("done \n\n\n");
}


/**************************************************************************/
/*!
    This function is called whenever select ACI events happen
*/
/**************************************************************************/
void aciCallback(aci_evt_opcode_t event)
{
  switch(event)
  {
    case ACI_EVT_DEVICE_STARTED:
      Serial.println(F("Bluetooth Advertising started"));
      break;
    case ACI_EVT_CONNECTED:
      Serial.println(F("Connected!"));
      break;
    case ACI_EVT_DISCONNECTED:
      Serial.println(F("Disconnected or advertising timed out"));
      break;
    default:
      break;
  }
}

/**************************************************************************/
/*!
    This function is called whenever data arrives on the RX channel
*/
/**************************************************************************/
void rxCallback(uint8_t *buffer, uint8_t len)
{
  long seconds = 0;
  Serial.print("converting seconds:\n");
  for(int i=0; i<len; i++)
  {
    if (buffer[i] < 48 || buffer[i] > 57){
      Serial.println("Please only use numbers.");
      uart.println("Please only use numbers.");
      return;
    }
    seconds = (seconds * 10) + (buffer[i] - 48) ;
  }

  Serial.print("Setting time between readings to ");
  Serial.print(seconds);
  Serial.println(" seconds");
  uart.print("Setting time between readings to ");
  uart.print(seconds);
  uart.print(" seconds");

  intervalBetweenReadings = seconds * 1000;
}


void populateLUT(){
  // only sets the values with one bit on in a two byte array
  unsigned int val = 1;
  for(int i=0; i<sensorCount; i++){
    setSensorLUTValue(val, i);
    val = val * 2;
  }
}

void milli_delay(int delay){
  long start_time = millis();
  while (start_time + delay > millis())
  {
    // hold here while we delay
  }
  
}

void setSensorLUTValue(unsigned int value, int index) {

  Serial.print(index);
  Serial.print(" : ");
  Serial.print(value);
  Serial.print(" -> ");
  Serial.print("low byte: ");
  Serial.print(lowByte(value));
  Serial.print("high byte: ");
  Serial.print(highByte(value));
  Serial.print("\n");

  
  SensorPinLUT[index][0] = lowByte(value);
  SensorPinLUT[index][1] = highByte(value);
}

void loop() {
  // put your main code here, to run repeatedly:
  uart.pollACI();

  if(millis() - previousMillis > intervalBetweenReadings){
    for (int i = 0; i < sensorCount; i++){
      pollSensor(i);
      }
    indicateEndOfMessage();
    previousMillis = millis();
  }

}

void indicateEndOfMessage(){
  uart.print("\n");
}

void pollSensor(int index){
  setSensorOn(index);
  
  milli_delay(delayBetweenSensor); // delay to stabilize the reading

  int sensorVal = readSensor();
  reportValue(sensorVal, index);

  setSensorOff();
  milli_delay(delayBetweenSensor);
}

int readSensor(){
  int reading = analogRead(SensorPin);
  Serial.print("raw value: ");
  Serial.println(reading);
  // convert to %
  reading = map(reading, air_value, water_value, 0, 100);
  return reading;
}

void reportValue(int reading, int sensorIndex){
  Serial.print("Sensor #");
  Serial.print(sensorIndex);
  Serial.print(" had a value of ");
  Serial.println(reading);

  // bleSerial.print("Sensor ");
  uart.print(sensorIndex);
  uart.print(" , ");
  uart.print(reading);
  uart.print(" . ");
  Serial.println("ble report done");
}

void setSensorOn(int index){
  digitalWrite(latchPin, 0);
  shiftOut(dataPin, clockPin, 255);
  shiftOut(dataPin, clockPin, SensorPinLUT[index][0]);
  shiftOut(dataPin, clockPin, SensorPinLUT[index][1]);  
  digitalWrite(latchPin, 1);
}

void setSensorOff(){
    digitalWrite(latchPin, 0);
    shiftOut(dataPin, clockPin, 0);
    shiftOut(dataPin, clockPin, 0);
    digitalWrite(latchPin, 1);
}

void shiftOut(int myDataPin, int myClockPin, byte myDataOut) {
  // This shifts 8 bits out MSB first,
  //on the rising edge of the clock,
  //clock idles low
  //internal function setup
  int i=0;
  int pinState;
  pinMode(myClockPin, OUTPUT);
  pinMode(myDataPin, OUTPUT);
  //clear everything out just in case to
  //prepare shift register for bit shifting
  digitalWrite(myDataPin, 0);
  digitalWrite(myClockPin, 0);
  //for each bit in the byte myDataOut&#xFFFD;
  //NOTICE THAT WE ARE COUNTING DOWN in our for loop
  //This means that %00000001 or "1" will go through such
  //that it will be pin Q0 that lights.
  for (i=7; i>=0; i--)  {
    digitalWrite(myClockPin, 0);
    //if the value passed to myDataOut and a bitmask result
    // true then... so if we are at i=6 and our value is
    // %11010100 it would the code compares it to %01000000
    // and proceeds to set pinState to 1.
    if ( myDataOut & (1<<i) ) {
      pinState= 1;
    }
    else {
      pinState= 0;
    }
    //Sets the pin to HIGH or LOW depending on pinState
    digitalWrite(myDataPin, pinState);
    //register shifts bits on upstroke of clock pin
    digitalWrite(myClockPin, 1);
    //indexero the data pin after shift to prevent bleed through
    digitalWrite(myDataPin, 0);
  }
  //stop shifting
  digitalWrite(myClockPin, 0);
}

void blink() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);   
}
