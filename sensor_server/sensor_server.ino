// Load Wi-Fi library
#include <WiFi.h>
#include <WebServer.h>
#include <SPI.h>
#include <string.h>

//Pin connected to ST_CP of 74HC595
int latchPin = 17;
//Pin connected to SH_CP of 74HC595
int clockPin = 5;
////Pin connected to DS of 74HC595
int dataPin = 16;
////Pin connected to sensor output
const int SensorPin = 33;

// 8 * the ammount of registers you have. 
// used to allow the loop to read all sensors
const int sensorCount = 16;

//Time between readings in miliseconds, 
// meant to stabilize the reading
int delayBetweenSensor = 100;

// network credentials
const char* ssid     = "TP-LINK_B124";
const char* password = "69795299";

// Set web server port number to 80
WebServer server(80);

// Set your Static IP address
IPAddress local_IP(192, 168, 0, 106);
// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);

IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional


// set sleep constants
int SleepSecs = 1;
int uS_TO_S_FACTOR = 1000000;

void setup() {
  //set sensor pins
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(SensorPin, INPUT);

  adcAttachPin(SensorPin);
  setSensorOff();
  
  Serial.begin(115200);


  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  
  Serial.println(WiFi.localIP());

  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");

  // set up sleep mode
  esp_sleep_enable_timer_wakeup(SleepSecs * uS_TO_S_FACTOR);
}

void loop() {
  server.handleClient();
}

void handle_OnConnect() {
  String data = GetSensorData();
  Serial.println(data);
  server.send(200, "text/plain", data); 
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String GetSensorData(){
  String ptr = String("{");
  for (int i = 0; i < sensorCount; i++){
      if(i>0){
        ptr = String(ptr + ", ");
        }
      ptr = String(ptr + getSensorString(i));
      }
      ptr = String(ptr + "}");
  setSensorOff();
  return ptr;
}

String getSensorString(int index){
  setSensorOn(index);
  
  delay(delayBetweenSensor); // delay to stabilize the reading

  int sensorVal = readSensor();
  String data = format_data(sensorVal, index);

  return data;
}

int readSensor(){
  int reading = analogRead(SensorPin);
  Serial.print("raw value: ");
  Serial.println(reading);
  return reading;
}

// formats the data of the reading as a json entry
// such as 1: "123", 
String format_data(int reading, int sensorIndex){
  Serial.print("Sensor #");
  Serial.print(sensorIndex);
  Serial.print(" had a value of ");
  Serial.println(reading);
  String data = String("\"");
  data = String(data + sensorIndex + "\": " + reading);
  return data;
}

void setSensorOn(int index){
  digitalWrite(latchPin, 0);
  uint16_t byte_num = pow(2, index);
  shiftOut(dataPin, clockPin, byte_num);
  digitalWrite(latchPin, 1);
}

void setSensorOff(){
    digitalWrite(latchPin, 0);
    shiftOut(dataPin, clockPin, 0);
    shiftOut(dataPin, clockPin, 0);
    digitalWrite(latchPin, 1);
}


void shiftOut(int myDataPin, int myClockPin, byte myDataOut) {
  // This shifts 8 bits out MSB first,
  //on the rising edge of the clock,
  //clock idles low
  //internal function setup
  int i=0;
  int pinState;
  pinMode(myClockPin, OUTPUT);
  pinMode(myDataPin, OUTPUT);
  //clear everything out just in case to
  //prepare shift register for bit shifting
  digitalWrite(myDataPin, 0);
  digitalWrite(myClockPin, 0);
  //for each bit in the byte myDataOut&#xFFFD;
  //NOTICE THAT WE ARE COUNTING DOWN in our for loop
  //This means that %00000001 or "1" will go through such
  //that it will be pin Q0 that lights.
  for (i=7; i>=0; i--)  {
    digitalWrite(myClockPin, 0);
    //if the value passed to myDataOut and a bitmask result
    // true then... so if we are at i=6 and our value is
    // %11010100 it would the code compares it to %01000000
    // and proceeds to set pinState to 1.
    if ( myDataOut & (1<<i) ) {
      pinState= 1;
    }
    else {
      pinState= 0;
    }
    //Sets the pin to HIGH or LOW depending on pinState
    digitalWrite(myDataPin, pinState);
    //register shifts bits on upstroke of clock pin
    digitalWrite(myClockPin, 1);
    //indexero the data pin after shift to prevent bleed through
    digitalWrite(myDataPin, 0);
  }
  //stop shifting
  digitalWrite(myClockPin, 0);
}
