import pytest

from moisture_sensor.sensors import sensors
from moisture_sensor import _database
from test import fixtures

session = fixtures.get_test_db_session()


class TestSensor:
    name = "violet"
    pin_number = 0

    def clear_table(self):
        session.query(_database.Sensor).delete()
        # session.query(_database.SensorReading).delete()
        session.commit()

    def test_add_sensor(self):
        self.clear_table()
        name = "violet"
        pin_number = 0
        sensors.add_sensor(session=session, pin_number=pin_number, name=name, active=True)

        all_sensors = sensors.list_all_sensors(session=session, include_inactive=False)
        assert all_sensors[0]["name"] == name
        assert all_sensors[0]["pin_number"] == pin_number
        assert all_sensors[0]["active"] is True
        assert all_sensors[0]["id"]
        assert len(all_sensors) == 1

    def test_list_sensor_exclude_inactive(self):
        self.clear_table()

        sensors.add_sensor(session=session, pin_number=self.pin_number, name="dead plant", active=False)
        sensors.add_sensor(session=session, pin_number=self.pin_number, name=self.name, active=True)

        all_sensors = sensors.list_all_sensors(session=session, include_inactive=False)
        assert all_sensors[0]["name"] == self.name
        assert all_sensors[0]["pin_number"] == self.pin_number
        assert all_sensors[0]["active"] is True
        assert all_sensors[0]["id"]
        assert len(all_sensors) == 1

    def test_modify_sensor_name(self):
        self.clear_table()
        sensors.add_sensor(session=session, pin_number=self.pin_number, name=self.name, active=True)

        old_name = sensors.get_sensor(session=session, pin_number=self.pin_number)

        tomatoes = "tomatoes"
        sensors.rename_sensor(session=session, pin_number=self.pin_number, new_name=tomatoes)

        new_name = sensors.get_sensor(session=session, pin_number=self.pin_number)

        assert new_name["name"] == tomatoes
        assert old_name["pin_number"] == new_name["pin_number"]
        assert old_name["active"] == new_name["active"]
        assert old_name["id"] == new_name["id"]

    def test_modify_pin_number(self):
        self.clear_table()
        sensors.add_sensor(session=session, pin_number=self.pin_number, name=self.name, active=True)

        old_name = sensors.get_sensor(session=session, pin_number=self.pin_number)

        new_pin = 5
        sensors.change_sensor_pin(session=session, pin_number=self.pin_number, new_pin=new_pin)

        new_name = sensors.get_sensor(session=session, pin_number=new_pin)

        assert new_name["pin_number"] == new_pin
        assert new_name["name"] == new_name["name"]
        assert old_name["active"] == new_name["active"]
        assert old_name["id"] == new_name["id"]

    def test_deactivate_sensor(self):
        self.clear_table()
        sensors.add_sensor(session=session, pin_number=self.pin_number, name=self.name, active=True)

        old_name = sensors.get_sensor(session=session, pin_number=self.pin_number)

        sensors.deactivate_sensor(session=session, pin_number=self.pin_number)

        new_name = sensors.list_all_sensors(session=session, include_inactive=True)[0]

        assert new_name["pin_number"] == self.pin_number
        assert new_name["name"] == new_name["name"]
        assert new_name["active"] is False
        assert old_name["id"] == new_name["id"]

    def test_rise_error_on_duplicate_pin(self):
        self.clear_table()
        sensors.add_sensor(session=session, pin_number=self.pin_number, name=self.name, active=True)

        with pytest.raises(ValueError):
            sensors.add_sensor(session=session,
                               pin_number=self.pin_number,
                               name=self.name, active=True)

    def test_rise_on_inexistent_pin(self):
        self.clear_table()
        with pytest.raises(ValueError):
            sensors.get_sensor(session=session,
                               pin_number=99999)

    def test_new_sensor_active_by_default(self):
        self.clear_table()
        name = "violet"
        pin_number = 10
        sensors.add_sensor(session=session, pin_number=pin_number, name=name)

        all_sensors = sensors.list_all_sensors(session=session, include_inactive=False)
        assert all_sensors[0]["name"] == name
        assert all_sensors[0]["pin_number"] == pin_number
        assert all_sensors[0]["active"] is True
        assert all_sensors[0]["id"]


if __name__ == "__main__":
    pytest.main()
