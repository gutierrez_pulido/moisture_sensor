import pytest
import json

from moisture_sensor.recieve_readings import parser


class MockRegister(parser.RegisterInterface):
    register = []

    def make_record(self, value):
        self.register.append(value)


class TestBlueToothHandler:
    register = MockRegister()

    def test_recieves_characters(self):
        handler = self.get_handler()
        handler.recieve_data(0, b"half of")

        assert handler.message == "half of"

    def test_recieve_multiple(self):
        handler = self.get_handler()
        handler.recieve_data(0, b"half of")
        handler.recieve_data(0, b" a message")

        assert handler.message == "half of a message"

    def test_line_is_finished(self):
        handler = self.get_handler()
        handler.recieve_data(0, b"half of")
        handler.recieve_data(0, b" a message")
        handler.recieve_data(0, b"\n")

        assert handler.message == ""
        assert self.register.register[-1] == "half of a message"

    def get_handler(self):
        self.register = MockRegister()
        handler = parser.BluetoothLineHandler(register=self.register)
        return handler


class TestJsonHandler:

    def test_data_is_formatted_for_register(self):
        handler = self.get_handler()
        data = {1: 12, 2:24}
        handler.recieve_data(1, bytes(json.dumps(data), encoding="ascii"))

        assert self.register.register[-1] == "1, 12.2, 24."

    def test_handler_is_callable(self):
        handler = self.get_handler()
        data = {1: 12, 2: 24}
        handler(1, bytes(json.dumps(data), encoding="ascii"))

        assert self.register.register[-1] == "1, 12.2, 24."

    def get_handler(self):
        # type: () -> parser.JSONDictHandler
        self.register = MockRegister()
        handler = parser.JSONDictHandler(register=self.register)
        return handler


if __name__ == '__main__':
    pytest.main()
