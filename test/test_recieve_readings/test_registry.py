import pytest
from moisture_sensor.recieve_readings import register_readings
from collections import defaultdict
import datetime


class MockDatabase(register_readings.ReadingDBInterface):
    data = defaultdict(list)

    def add_reading(self, sensor, reading, date_of_reading):
        self.data[sensor].append([reading, date_of_reading])


class TestRegisterReading:
    db = MockDatabase()
    SENSOR_OUTPUT = "0 , 10 . 1 , 20 . "   # this format is what comes out of the sensors. hard to change

    def test_register_reading(self):
        register = self.get_register()

        register.make_record(self.SENSOR_OUTPUT)
        # sensor number 0 has a value of 10 on __date__
        assert self.db.data[0] == [[10, "__date__"]]
        # sensor number 1 has a value of 20 on __date__
        assert self.db.data[1] == [[20, "__date__"]]

    def test_date(self):
        self.db = MockDatabase()
        register = register_readings.RegisterReading(db_interactor=self.db)
        assert isinstance(register.get_date(), datetime.datetime)

    def get_register(self):
        self.db = MockDatabase()
        get_date = lambda *args, **kwargs: "__date__"
        register = register_readings.RegisterReading(db_interactor=self.db)
        register.get_date = get_date
        return register


if __name__ == '__main__':
    pytest.main()