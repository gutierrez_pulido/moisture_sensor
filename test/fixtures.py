from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from moisture_sensor import _database


def get_test_db_session():
    engine = create_engine('sqlite://', echo=True)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    _database.connect(engine)
    return session


def raw_to_percent(value):
    """converts raw reading to percent on default values"""
    sensor_min = 2000
    sensor_max = 800
    out_min = 0
    out_max = 100
    return (value - sensor_min) * (out_max - out_min) / (sensor_max - sensor_min) + out_min
