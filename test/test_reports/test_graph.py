import pytest
from unittest import mock
import os

import moisture_sensor.reports
from . import report_fixtures
from moisture_sensor.reports import format_data, email


class TestFormatAsGraph:
    image_path = "/tmp/plot.png"

    def clean_image(self):
        try:
            os.remove(self.image_path)
        except FileNotFoundError:
            pass

    def test_format_graph_and_readings(self):
        self.clean_image()

        try:
            user_data = report_fixtures.MockDataGather("", "")
            plot = format_data.PlotReadings(user_data, self.image_path)
            plot.save_to(self.image_path)

            assert os.path.exists(self.image_path)
        finally:
            self.clean_image()

    def test_class_has_show_method(self):
        with mock.patch.object(format_data.pyplot, "show") as mock_show:
            user_data = report_fixtures.MockDataGather("", "")
            plot = format_data.PlotReadings(user_data, self.image_path)
            plot.show()

        print(mock_show.mock_calls)
        # assert that pyplot.show has been called
        assert mock_show.call_count

        # plot.show()

    def test_plot_clears_on_exit(self):
        with mock.patch.object(format_data.pyplot, "close") as mock_close:
            user_data = report_fixtures.MockDataGather("", "")
            plot = format_data.PlotReadings(user_data, self.image_path)
            del plot
            assert mock_close.call_count

    def test_hierarchy(self):
        assert issubclass(format_data.PlotReadings, moisture_sensor.reports.PlotInterface)


if __name__ == "__main__":
    pytest.main()
