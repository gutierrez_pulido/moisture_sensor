import pytest
from ..fixtures import get_test_db_session

from moisture_sensor.reports import graph_lists, settings
from moisture_sensor.sensors.sensors import add_sensor

session = get_test_db_session()

TEST_EMAIL = "a@b.c"
TEST_SENSORS = [0, 1, 2]


def _setup_db(session_):
    setts = settings.SettingDBInteractor(session_)
    setts.add_new_user(email=TEST_EMAIL, first_name="me")

    add_sensor(session, pin_number=0, name="violets")
    add_sensor(session, pin_number=1, name="tomatoes")
    add_sensor(session, pin_number=2, name="chillies")

    for pin in TEST_SENSORS:
        setts.subscribe_to_sensor(user_email=TEST_EMAIL, sensor_pin=pin)


_setup_db(session)


class TestGraphLists:
    user_id = 0
    setts = graph_lists.GraphListDBInteractor(session)

    def test_creating_list(self):
        list_name = "dry_plants"
        sensors = [0, 2]
        self.setts.add_new_graph_list(
            user_email=TEST_EMAIL, sensor_pins=sensors, list_name=list_name)
        my_list = self.get_list(list_name)

        assert my_list.sensors == sensors

    def test_add_pins_to_existing_list(self):
        list_name = "test"
        sensors = [0]
        self.setts.add_new_graph_list(
            user_email=TEST_EMAIL, sensor_pins=sensors, list_name=list_name)

        self.setts.add_pins_to_graph_list(
            user_email=TEST_EMAIL, list_name=list_name, sensor_pins=[1, 2])

        my_list = self.get_list(list_name)
        assert my_list.sensors == [0, 1, 2]

    def get_list(self, list_name):
        lists = self.setts.get_user_graph_lists(user_email=TEST_EMAIL)
        my_list = None
        for list in lists:
            if list.name == list_name:
                my_list = list
                break
        return my_list

    def test_list_errors_on_duplicate_name(self):
        list_name = "Unique_name"
        sensors = [0]
        self.setts.add_new_graph_list(
            user_email=TEST_EMAIL, sensor_pins=sensors, list_name=list_name)

        with pytest.raises(ValueError):
            self.setts.add_new_graph_list(
                user_email=TEST_EMAIL, sensor_pins=sensors,
                list_name=list_name)

    def test_does_not_error_on_duplicate_name_diff_user(self):
        user_email = "different@email.com"
        setts = settings.SettingDBInteractor(session)
        setts.add_new_user(email=user_email, first_name="totally not me")

        list_name = "second_name"
        sensors = [0]
        self.setts.add_new_graph_list(
            user_email=TEST_EMAIL, sensor_pins=sensors, list_name=list_name)

        self.setts.add_new_graph_list(
            user_email=user_email,
            sensor_pins=sensors,
            list_name=list_name)


if __name__ == '__main__':
    pytest.main()
