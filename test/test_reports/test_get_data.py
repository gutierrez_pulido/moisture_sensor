import pytest
from datetime import date

from moisture_sensor.reports import user_data
from moisture_sensor.reports.format_data import UserDataInterface
from moisture_sensor.reports import graph_lists

from test.fixtures import get_test_db_session, raw_to_percent
from test.test_reports.report_fixtures import \
    EMAIL, YESTERDAY_DATE, DAY_BEFORE_DATE, EMAIL_FREQUENCY, setup_db, old_date

session = get_test_db_session()
# setup the database
setup_db(session)

setts = graph_lists.GraphListDBInteractor(session)
LIST_NAME = "list_name"
setts.add_new_graph_list(
    user_email=EMAIL, sensor_pins=[0], list_name=LIST_NAME)

class TestDataGather:
    violet_sensor = 0
    tomatoes_sensor = 1

    def test_get_subscribed_sensors_readings(self):
        data_object = self.get_data_gather()

        assert data_object.readings[self.violet_sensor] == [
            [DAY_BEFORE_DATE, raw_to_percent(10)], [YESTERDAY_DATE, raw_to_percent(20)], [old_date, raw_to_percent(1)]]
        assert data_object.readings[self.tomatoes_sensor] == [
            [DAY_BEFORE_DATE, raw_to_percent(15)], [YESTERDAY_DATE, raw_to_percent(25)], [old_date, raw_to_percent(1)]]

    def test_get_sensors(self):
        data_object = self.get_data_gather()
        assert data_object.subscribed_sensors == [
            {'active': True, 'id': 1, 'name': 'violet', 'pin_number': 0},
            {'active': True, 'id': 2, 'name': 'Tomatoes', 'pin_number': 1}]

    def test_email_frequency(self):
        data_object = self.get_data_gather()
        assert data_object.email_frequency == EMAIL_FREQUENCY

    def test_email(self):
        data_object = self.get_data_gather()
        assert data_object.email == EMAIL

    def test_user_name(self):
        data_object = self.get_data_gather()
        assert data_object.first_name == "John"
        assert not data_object.last_name

    def test_missing_user_raises(self):
        with pytest.raises(ValueError):
            data_object = user_data.AllUserSensors(
                user_email="missing@mail.com", session=session)

    def get_data_gather(self):
        data_object = user_data.AllUserSensors(user_email=EMAIL,
                                               session=session)

        # patch date to return always the same date
        data_object._start_date = lambda *x, **y: date(2020, 12, 25)
        return data_object

    def test_hierarchy(self):
        data_object = self.get_data_gather()
        assert issubclass(type(data_object), UserDataInterface)


class TestUserMailList:
    def test_mail_list_returns_correct_sensors(self):
        data_object = user_data.UserMailList(
            session=session, user_email=EMAIL, mail_list=LIST_NAME)

        assert data_object.subscribed_sensors == \
               [{'active': True, 'id': 1, 'name': 'violet', 'pin_number': 0}]

    def test_mail_list_raises_on_missing_list(self):
        with pytest.raises(ValueError):
            data_object = user_data.UserMailList(
                session=session, user_email=EMAIL,
                mail_list="tomato_plantsn")

    def test_mail_list_recieves_graphlist_object(self):
        graph = user_data.GraphList(name=LIST_NAME, list_id=1,
                                    user_id=1, sensors=[0])
        data_object = user_data.UserMailList(
            session=session, user_email=EMAIL, mail_list=graph)

        assert data_object.mail_list == LIST_NAME

if __name__ == "__main__":
    pytest.main()

