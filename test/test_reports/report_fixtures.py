from datetime import timedelta, datetime
from copy import copy

from moisture_sensor.readings import readings
from moisture_sensor.reports import format_data, settings
from moisture_sensor.sensors import sensors
from moisture_sensor import _database

from app_secrets import EMAIL_PASSWORD, API_EMAIL
ACTUAL_SUBSCRIBED_MAIL = "test@123.com"

EMAIL_FREQUENCY = 7
DAY_BEFORE_DATE = datetime.today() - timedelta(days=2)
YESTERDAY_DATE = datetime.today() - timedelta(days=1)
TODAY_DATE = datetime.today()
old_date = datetime.today() - timedelta(days=50)

EMAIL = ACTUAL_SUBSCRIBED_MAIL
SENDER_EMAIL = API_EMAIL
SENDER_PASS = EMAIL_PASSWORD


class MockDataGather(format_data.UserDataInterface):
    def __init__(self, session, email):
        super(MockDataGather, self).__init__(session, email)
        self.subscribed_sensors = [
            {'active': True, 'id': 1, 'name': 'violet', 'pin_number': 0},
            {'active': True, 'id': 2, 'name': 'Tomatoes', 'pin_number': 1}]
        self.email_frequency = copy(EMAIL_FREQUENCY)
        self.email = copy(EMAIL)

    @property
    def readings(self) -> dict:
        return {0: [[DAY_BEFORE_DATE, 10], [YESTERDAY_DATE, 20]],
                1: [[DAY_BEFORE_DATE, 15], [YESTERDAY_DATE, 25]],
                }


def add_sensors(session):
    sensors.add_sensor(session=session, pin_number=0, name="violet")
    sensors.add_sensor(session=session, pin_number=1, name="Tomatoes")
    sensors.add_sensor(session=session, pin_number=2,
                       name="dead plant")


def add_readings(session):
    reading = readings.SensorReadingDBInteractor(session)
    #violet
    reading.add_reading(sensor=0, reading=10, date_of_reading=DAY_BEFORE_DATE)
    reading.add_reading(sensor=0, reading=20, date_of_reading=YESTERDAY_DATE)
    reading.add_reading(sensor=0, reading=1, date_of_reading=old_date)
    # tomatoes
    reading.add_reading(sensor=1, reading=15, date_of_reading=DAY_BEFORE_DATE)
    reading.add_reading(sensor=1, reading=25, date_of_reading=YESTERDAY_DATE)
    reading.add_reading(sensor=1, reading=1, date_of_reading=old_date)
    # dead plant
    reading.add_reading(sensor=2, reading=00, date_of_reading=DAY_BEFORE_DATE)
    reading.add_reading(sensor=2, reading=00, date_of_reading=YESTERDAY_DATE)
    reading.add_reading(sensor=2, reading=00, date_of_reading=old_date)


def add_user_settings(session):
    setts = settings.SettingDBInteractor(session)
    setts.add_new_user(email=EMAIL, first_name="John")
    setts.set_email_frequency(EMAIL, frequency=EMAIL_FREQUENCY)
    setts.subscribe_to_sensor(EMAIL, sensor_pin=0)
    setts.subscribe_to_sensor(EMAIL, sensor_pin=1)
    setts.subscribe_to_sensor(EMAIL, sensor_pin=2)


def kill_sensor(session):
    sensors.deactivate_sensor(session, pin_number=2)


def clear_table(session):
    user = session.query(_database.User).get(1)
    if user:
        user.sensor_subscriptions = []
        session.commit()

    session.query(_database.User).delete()
    session.query(_database.Reading).delete()
    session.query(_database.Sensor).delete()
    session.query(_database.Settings).delete()
    session.commit()


def setup_db(session):
    list(map(lambda x: x(session),
             [add_sensors, add_readings, add_user_settings, kill_sensor]))