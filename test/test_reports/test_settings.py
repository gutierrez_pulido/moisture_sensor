import pytest

from moisture_sensor.reports import settings
from moisture_sensor.sensors import sensors
from moisture_sensor import _database

from test import fixtures

session = fixtures.get_test_db_session()

# add a couple sensors
sensors.add_sensor(session, 0, "violet", active=False)
sensors.add_sensor(session, 0, "Pink violet", active=True)
sensors.add_sensor(session, 1, "tomato", active=True)


class TestEmailSettings:
    email = "my@email.com"
    first_name = "John"
    last_name = "Cena"

    def clear_table(self):
        try:
            session.rollback()
        except:
            pass
        # clean sensor subscriptions
        user = session.query(_database.User).get(1)
        if user:
            user.sensor_subscriptions = []
            session.commit()

        session.query(_database.User).delete()
        session.query(_database.Settings).delete()
        session.commit()

    def test_add_new_user(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)

        setts.add_new_user(email=self.email, first_name=self.first_name, last_name=self.last_name)

        assert setts.list_users()[0]["email"] == self.email
        assert setts.list_users()[0]["first_name"] == self.first_name
        assert setts.list_users()[0]["last_name"] == self.last_name

    def test_optional_last_name(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)

        setts.add_new_user(email=self.email, first_name=self.first_name)

        assert setts.list_users()[0]["email"] == self.email
        assert setts.list_users()[0]["first_name"] == self.first_name
        assert not setts.list_users()[0]["last_name"]

    def test_duplicate_email_errors(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)

        setts.add_new_user(email=self.email, first_name=self.first_name)
        with pytest.raises(ValueError):
            setts.add_new_user(email=self.email, first_name=self.first_name)

    def test_set_email_frequency(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)
        setts.add_new_user(email=self.email, first_name=self.first_name)

        setts.set_email_frequency(user_email=self.email, frequency=7)

        data = setts.list_user_settings(user_email=self.email)
        assert data.email_frequency == 7
        assert data.email == self.email

    def test_default_frequency(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)
        setts.add_new_user(email=self.email, first_name=self.first_name)

        data = setts.list_user_settings(user_email=self.email)
        assert data.email_frequency == 1
        assert data.email == self.email

    def test_add_plant_subscription(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)
        setts.add_new_user(email=self.email, first_name=self.first_name)

        # # do the thing
        setts.subscribe_to_sensor(user_email=self.email, sensor_pin=0)
        setts.subscribe_to_sensor(user_email=self.email, sensor_pin=1)
        # setts.subscribe_to_sensor(user_email=self.email, sensor_pin=2)

        data = setts.list_user_settings(user_email=self.email)
        assert data.email_frequency == 1
        assert data.email == self.email

        found_pins = []
        for sense in data.subscribed_sensors:
            found_pins.append(sense["pin_number"])

        assert sorted(found_pins) == [0, 1]

    def test_plant_subscription_to_inactive_pins_does_not_show(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)
        setts.add_new_user(email=self.email, first_name=self.first_name)
        sensors.add_sensor(session, 2, "tomato", active=True)

        # subscribe
        setts.subscribe_to_sensor(user_email=self.email, sensor_pin=0)
        setts.subscribe_to_sensor(user_email=self.email, sensor_pin=1)
        setts.subscribe_to_sensor(user_email=self.email, sensor_pin=2)

        # deactivate pin
        sensors.deactivate_sensor(session=session, pin_number=2)

        data = setts.list_user_settings(user_email=self.email)

        found_pins = []
        for sense in data.subscribed_sensors:
            found_pins.append(sense["pin_number"])

        assert sorted(found_pins) == [0, 1]

        session.query(_database.Sensor).\
            filter(_database.Sensor.pin_number == 2).\
            filter(_database.Sensor.active == True).delete()
        session.commit()

    def test_double_subscription(self):
        self.clear_table()
        setts = settings.SettingDBInteractor(session=session)
        email = "random_email"
        setts.add_new_user(email=email, first_name=self.first_name)

        # # do the thing
        setts.subscribe_to_sensor(user_email=email, sensor_pin=0)
        setts.subscribe_to_sensor(user_email=email, sensor_pin=0)

        data = setts.list_user_settings(user_email=email)
        found_pins = []
        for sense in data.subscribed_sensors:
            found_pins.append(sense["pin_number"])

        assert sorted(found_pins) == [0]


if __name__ == "__main__":
    pytest.main()
