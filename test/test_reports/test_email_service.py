import pytest
from unittest import mock
from datetime import date, timedelta
from contextlib import contextmanager
from functools import partial

from moisture_sensor.reports import email_service

from test.fixtures import get_test_db_session
from .report_fixtures import setup_db

session = get_test_db_session()
setup_db(session)


class TestEmailService:

    def test_service_calls_send_email(self):
        with self.get_service() as serv:
            service, mocks = serv
            stop_running = partial(setattr, service, "running", False)
            service.send_emails = mock.MagicMock(**{"side_effect": stop_running})

            service.sleep_time = 1
            service.start()

            assert service.send_emails.called

    def test_has_trigger_timer(self):
        with self.get_service() as serv:
            service, mocks = serv
            assert service.trigger_every == timedelta(days=1)

            service.send_emails()
            assert service.last_trigger == date.today()

    def test_service_triggers_email(self):
        with self.get_service() as serv:
            service, mocks = serv
            service.send_emails()

    def test_send_email_triggers_email_sender(self):
        with self.get_service() as serv:
            service, mocks = serv
            service.send_emails()
            assert mocks["send_email_to_user"].message_sent

    @contextmanager
    def get_service(self):
        with mock.patch.object(email_service.email, "EmailSender") \
                as sender:
            email = mock.MagicMock()
            instance = mock.MagicMock(**{"send_email_to_user": email})
            sender.configure_mock(**{"return_value": instance})
            mocks = {"sender": sender, "send_email_to_user": email}
            service = email_service.EmailService(session=session)
            yield service, mocks


if __name__ == "__main__":
    pytest.main()
