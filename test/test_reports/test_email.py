import pytest
import os
from unittest import mock

import moisture_sensor.reports
from test import fixtures

from .report_fixtures import SENDER_EMAIL, \
    SENDER_PASS, setup_db, clear_table, EMAIL

from moisture_sensor.reports import email
from moisture_sensor.reports import graph_lists

session = fixtures.get_test_db_session()

# setup database data
setup_db(session)

setts = graph_lists.GraphListDBInteractor(session)
LIST_NAME = "list_name"
SECOND_NAME = "another!"
setts.add_new_graph_list(
    user_email=EMAIL, sensor_pins=[0], list_name=LIST_NAME)
setts.add_new_graph_list(
    user_email=EMAIL, sensor_pins=[0, 1, 2], list_name=SECOND_NAME)


class MockPlot(moisture_sensor.reports.PlotInterface):
    saved = []

    def __init__(self, user_data):
        pass

    def save_to(self, image_path):
        self.saved.append(image_path)
        with open(image_path, "bw") as fw:
            # create file
            pass

    def show(self):
        pass


class TestEmail:
    def clear_image(self, path):
        try:
            os.remove(path)
        except FileNotFoundError:
            pass

    def test_email_has_a_plot(self):
        mail = email.EmailForUser(session=session,
                                  reciever_email=EMAIL,
                                  )

        assert hasattr(mail, "plot_path")
        assert os.path.dirname(mail.plot_path) == "/tmp"
        assert os.path.exists(mail.plot_path)

        self.clear_image(path=mail.plot_path)

    def test_email_has_a_reciever(self):
        mail = email.EmailForUser(session=session,
                                  reciever_email=EMAIL,
                                  )
        assert mail.reciever == EMAIL

    def test_email_to_text(self):
        mail = email.EmailForUser(session=session,
                                  reciever_email=EMAIL,
                                  )
        assert mail.to_text()
        assert isinstance(mail.to_text(), str)
        # print(mail.to_text())


class TestEmailGraphList:
    def test_inits(self):
        data = email.EmailGraphList(session, reciever_email=EMAIL)

    def test_email_has_one_plot_per_graphlist(self):
        mail = email.EmailGraphList(session, reciever_email=EMAIL)
        assert len(mail.graph_lists) == 2
        assert len(mail.plot_paths) == 2

    def test_email_to_text(self):
        mail = email.EmailGraphList(session=session, reciever_email=EMAIL)
        assert mail.to_text()
        assert isinstance(mail.to_text(), str)


class TestEmailSender:
    def test_sends_connects(self):
        sender, mocks = self.get_sender()
        sender.connect()

        assert mocks["starttls"].called
        assert mocks["login"].called

    def get_sender(self):
        with mock.patch("moisture_sensor.reports.email.ssl"), \
                mock.patch("moisture_sensor.reports.email.smtplib"):
            sender = email.EmailSender(session=session,
                                       sender_email=SENDER_EMAIL,
                                       sender_pass=SENDER_PASS)
        magic_mock = mock.MagicMock()
        mocks = {"starttls": mock.MagicMock(),
                 "login": mock.MagicMock(),
                 "quit": mock.MagicMock(),
                 "sendmail": mock.MagicMock()}

        magic_mock.configure_mock(**mocks)
        sender.server = magic_mock
        sender.connect()
        return sender, mocks

    def test_disconnect_on_exit(self):
        sender, mocks = self.get_sender()
        del sender
        assert mocks["quit"].called

    def test_send_email_to_user(self):
        sender, mocks = self.get_sender()
        sender.send_email_to_user(email=EMAIL)
        assert mocks["sendmail"].called



# os.environ['DRY_RUN'] = "True"


@pytest.mark.skipif(os.environ.get('DRY_RUN') is None,
                    reason="meant for manual checking")
def test_dry_run():
    """actually sends an email for manual checking"""
    # clear_table(session)
    # setup_db(session)
    # example_mail = _get_email()

    sender = email.EmailSender(session=session,
                               sender_email=SENDER_EMAIL,
                               sender_pass=SENDER_PASS)
    sender.connect()
    sender.send_email_to_user("berna.92@gmail.com")


if __name__ == "__main__":
    pytest.main()
