import pytest
import os
from unittest import mock

try:
    from moisture_sensor import main as moist
except Exception:
    pass


@pytest.mark.skipif(os.name == "nt", reason="running from windows. expect failures")
class Test:
    def test_start_email_service(self):
        with mock.patch("moisture_sensor.main.EmailService") as email_mock:
            moist.send_email()
            assert email_mock.called

    def test_start_sensor(self):
        os.chdir("../")
        with mock.patch("moisture_sensor.main.recieve_bluetooth_sensor_data") as sense_mock:
            moist.start_bluetooth_reciever()
            assert sense_mock.called


if __name__ == "__main__":
    pytest.main()
