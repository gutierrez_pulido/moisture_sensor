import pytest
import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from moisture_sensor.readings.readings import SensorReadingDBInteractor
from moisture_sensor.recieve_readings import register_readings
from moisture_sensor import _database

engine = create_engine('sqlite://', echo=True)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()
_database.connect(engine)


# setup_db_data:
# add a couple of sensors to our test database
sensor1 = _database.Sensor(name="Pink violet", active=True,  pin_number=0)
sensor2 = _database.Sensor(name="Blue violet", active=True,  pin_number=1)
session.add_all([sensor1, sensor2])
session.commit()


class TestRegistryDatabase:
    EXAMPLE_DATE = datetime.datetime.fromisoformat("2020-12-26")
    _database.connect(engine)
    air_value_default = 2000
    water_value_default = 800

    def raw_to_percent(self, value):
        """converts raw reading to percent"""
        sensor_min = self.air_value_default
        sensor_max = self.water_value_default
        out_min = 0
        out_max = 100
        return (value - sensor_min) * (out_max - out_min) / (sensor_max - sensor_min) + out_min

    def clear_table(self):
        session.query(_database.Reading).delete()
        # session.query(_database.SensorReading).delete()
        sensor1.active = True
        sensor2.active = True

        session.commit()

    def test_hierarchy(self):
        db = SensorReadingDBInteractor
        assert issubclass(db, register_readings.ReadingDBInterface)

    def test_add_new_record(self,):
        self.clear_table()
        db = SensorReadingDBInteractor(session)
        db.add_reading(sensor=0, reading=10, date_of_reading=self.EXAMPLE_DATE)

        readings = db.get_readings(sensor=0, )
        assert readings[0]["value"] == self.raw_to_percent(10)
        assert readings[0]["date"] == self.EXAMPLE_DATE

    def test_add_multiple(self):
        self.clear_table()

        db = SensorReadingDBInteractor(session)
        db.add_reading(sensor=0, reading=10, date_of_reading=self.EXAMPLE_DATE)
        db.add_reading(sensor=0, reading=20, date_of_reading=self.EXAMPLE_DATE)
        db.add_reading(sensor=1, reading=20, date_of_reading=self.EXAMPLE_DATE)

        readings = db.get_readings(sensor=0, )
        print(readings)
        assert readings[0]["value"] == self.raw_to_percent(10)
        assert readings[0]["date"] == self.EXAMPLE_DATE
        assert len(readings) == 2

        assert readings[1]["value"] == self.raw_to_percent(20)
        assert readings[1]["date"] == self.EXAMPLE_DATE

        readings = db.get_readings(sensor=1, )
        assert len(readings) == 1
        assert readings[0]["value"] == self.raw_to_percent(20)
        assert readings[0]["date"] == self.EXAMPLE_DATE

    def test_add_reading_does_not_add_to_inactive_sensors(self):
        self.clear_table()

        # add readings to a sensor
        db = SensorReadingDBInteractor(session)
        db.add_reading(sensor=0, reading=10, date_of_reading=self.EXAMPLE_DATE)
        db.add_reading(sensor=0, reading=20, date_of_reading=self.EXAMPLE_DATE)

        # deactivate sensor
        sensor1.active = False
        sensor3 = _database.Sensor(name="Dead plant", active=True, pin_number=0)
        session.add(sensor3)
        session.commit()

        # add readings to a new sensor on the same pin
        db.add_reading(sensor=0, reading=30, date_of_reading=self.EXAMPLE_DATE)

        readings = db.get_readings(sensor=0)
        assert len(readings) == 1
        assert readings[0]["value"] == self.raw_to_percent(30)
        assert readings[0]["date"] == self.EXAMPLE_DATE

        session.query(_database.Sensor).\
            filter(_database.Sensor.sensor_id == sensor3.sensor_id).delete()
        session.commit()

    def test_get_reading_from_inactive_sensor(self):
        self.clear_table()
        db = SensorReadingDBInteractor(session)

        sensor1.active = False
        session.commit()
        with pytest.raises(ValueError):
            readings = db.get_readings(sensor=sensor1.pin_number)

    def test_readings_after_a_date(self):
        self.clear_table()

        second_date = datetime.date.fromisoformat("2020-12-30")
        example_date_plus_one = datetime.date(2020, 12, 27)
        db = SensorReadingDBInteractor(session)
        db.add_reading(sensor=0, reading=10, date_of_reading=self.EXAMPLE_DATE)
        db.add_reading(sensor=0, reading=20, date_of_reading=self.EXAMPLE_DATE)
        db.add_reading(date_of_reading=second_date, sensor=0, reading=10)

        readings = db.get_readings(sensor=0, after_date=example_date_plus_one)

        for read in readings:
            assert read["date"] > self.EXAMPLE_DATE



class TestSensorTable:
    def test_table_is_created(self):
        engine = create_engine('sqlite://')
        _database.connect(engine)

        assert engine.has_table("Sensor")
        assert engine.has_table("Reading")


if __name__ == "__main__":
    pytest.main()
