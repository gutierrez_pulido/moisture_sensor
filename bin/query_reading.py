from pprint import pprint
import moisture_sensor
from moisture_sensor._database import get_db_session
from moisture_sensor.readings.readings import SensorReadingDBInteractor


db = SensorReadingDBInteractor(get_db_session())

for pin in range(0,16):
    try:
        readings = db.get_readings(sensor=pin)
        pprint(readings)
    except:
        pass



