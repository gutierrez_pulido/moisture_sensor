from pprint import pprint
import moisture_sensor
from moisture_sensor import _database
from moisture_sensor._database import get_db_session
from moisture_sensor.readings.readings import SensorReadingDBInteractor

session = get_db_session()
db = SensorReadingDBInteractor(session)

for pin in range(0, 16):
    try:
        readings = db.get_readings(sensor=pin)
        pprint(readings)
        sensor_in_table = session.query(_database.Sensor).filter(_database.Sensor.pin_number == pin).filter(_database.Sensor.active == True).one()
        reading_query = session.query(_database.Reading).filter(_database.Reading.sensor_id == sensor_in_table.sensor_id).all()
        [session.delete(each) for each in reading_query]
        session.commit()
    except:
        pass


