import sys

for each in sys.path:
    print(each)

import moisture_sensor
from moisture_sensor import main
import logging

def get_logger():
    format = "%(asctime)s: %(message)s"

    logging.basicConfig(filename="crontab.log", format=format, level=logging.DEBUG,
                        datefmt="%H:%M:%S")

    # create logger with 'spam_application'
    logger = logging.getLogger('moisture_sensor')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler("crontab.log")
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger

logger = get_logger()
main.read_sensor_server()


