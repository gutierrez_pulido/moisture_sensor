
cd /home/pi/Dev/moisture_sensor
echo $(date "+%Y-%m-%d %H:%M:%S" )
source ./venv/bin/activate
echo "running from " $(pwd)

export PYTHONPATH="$PYTHONPATH:/home/pi/Dev/moisture_sensor:/home/pi/Dev/moisture_sensor/venv/lib/python3.8/site-packages"
echo $PYTHONPATH
echo "-----"

/home/pi/Dev/moisture_sensor/venv/bin/python ./bin/send_email.py


