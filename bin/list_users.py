import moisture_sensor
from moisture_sensor._database import User, get_db_session


if __name__ == '__main__':
    session = get_db_session()
    for user in session.query(User):
        id = user.user_id
        mail = user.email
        subcriptions = ["{id} - {name} -- active: {active}".format(id=sub.sensor_id, name=sub.name, active=sub.active)
                        for sub in user.sensor_subscriptions]
        settings = ["settings: \n\t email frequency: {}\n\t days in graph: {}".format(
            setting.email_frequency, setting.days_in_graph)
            for setting in user.settings]

        print("user.user_id={}".format(id))
        print("user.email={},".format(mail))
        print("user.sensor_subscriptions={},".format("\n\t -".join(subcriptions)))
        print("user.settings={}".format(settings))
        print("-----")
        


